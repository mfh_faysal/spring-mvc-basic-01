package mfh.jbc.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class RootController extends AbstractController {

	private static final Logger logger = LoggerFactory.getLogger(RootController.class);

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {

		logger.info("Welcome!");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("root");
		return modelAndView;
	}

}
